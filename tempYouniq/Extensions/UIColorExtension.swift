//
//  UIColorExtension.swift
//  tempYouniq
//

import UIKit

private let _YouniqBlueColor: UIColor = UIColor(red: 0.0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0)
private let _YouniqGreyColor: UIColor = UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
private let _YouniqLightGreyColor: UIColor = UIColor(red: 213.0/255.0, green: 213.0/255.0, blue: 213.0/255.0, alpha: 1.0)
private let _YouniqOrangeColor: UIColor = UIColor(red: 242.0/255.0, green: 149.0/255.0, blue: 0.0/255.0, alpha: 1.0)
private let _YouniqRedColor: UIColor = UIColor(red: 208.0/255.0, green: 2.0/255.0, blue: 27.0/255.0, alpha: 1.0)


extension UIColor {
    
    public static var youniqBlack:                 UIColor { return .black }
    public static var youniqWhite:                 UIColor { return .white }
    public static var youniqClear:                 UIColor { return .clear }
    public static var youniqBlue:                  UIColor { return _YouniqBlueColor }
    public static var youniqGrey:                  UIColor { return _YouniqGreyColor }
    public static var youniqLightGrey:             UIColor { return _YouniqLightGreyColor }
    public static var youniqOrange:                UIColor { return _YouniqOrangeColor }
    public static var youniqRed:                   UIColor { return _YouniqRedColor }
}
