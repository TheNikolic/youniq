//
//  UIApplicationExtensions.swift
//  tempYouniq
//

import UIKit

extension UIApplication {
    
    static var topViewController: UIViewController? {
        return topViewController()
    }
    
    static func showFullscreen(modalView: YQModalView, _ completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            if modalView.superview == nil {
                topViewController()?.view.addSubview(modalView)
                completion?()
            }
        }
    }
    
    static func hideFullscreen(modalView: YQModalView, _ completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            modalView.removeFromSuperview()
            completion?()
        }
    }
    
    private static func topViewController(_ baseController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = baseController as? UINavigationController {
            return navigationController
        }
        if let tabBarController = baseController as? UITabBarController {
            return tabBarController
        }
        if let presented = baseController?.presentedViewController {
            return topViewController(presented)
        }
        
        return baseController
    }
}
