//
//  YQLabel.swift
//  tempYouniq
//

import UIKit

class YQLabel: UILabel {
    
    
    static func multiline() -> YQLabel {
        let label: YQLabel = YQLabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        
        return label
    }
    
    public init() {
        super.init(frame: CGRect.zero)
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
    }
}
