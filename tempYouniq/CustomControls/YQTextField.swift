//
//  YQTextField.swift
//  tempYouniq
//

import UIKit

class YQTextField: UITextField {
    
    public init() {
        super.init(frame: CGRect.zero)
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        autocorrectionType = .no
        autocapitalizationType = .none
    }
}
