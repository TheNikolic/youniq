//
//  YQModalView.swift
//  tempYouniq
//

import UIKit

private struct Constants {
    static let backgroundColorAlphaValue: CGFloat = 0.5
    static let disabledAlpha: CGFloat = 0.5
    static let buttonHeight: CGFloat = 50
    static let customActionHeight: CGFloat = 50
}

class YQModalView: UIView {
    
    private let containerView: UIView = UIView()
    private let actionContainerView: UIView = UIView()
    private var confirmButton: YQButton = YQButton()
    private var cancelButton: YQButton = YQButton()
    
    private var confirmActionBlock: (() -> Void)? = nil
    private var cancelActionBlock: (() -> Void)? = nil
    private var size: CGSize = CGSize.zero
    private var childView: UIView? = nil
    
    static func viewWith(childView child: UIView) -> YQModalView {
        let modalView: YQModalView = YQModalView()
        modalView.childView = child
        modalView.containerView.addSubview(modalView.childView!)
        
        return modalView
    }
    
    init() {
        super.init(frame: UIScreen.main.bounds)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        
        backgroundColor = UIColor.youniqBlack.withAlphaComponent(Constants.backgroundColorAlphaValue)
        
        setupContainers()
        addConfirmButton()
        addCancelButton()
    }
    
    func layoutWith(size: CGSize) {
        self.size = CGSize(width: size.width, height: size.height)
        
        setupConstraints()
    }
    
    func addCancelAction(_ action: @escaping () -> Void) {
        cancelActionBlock = action
    }
    
    func addConfirmAction(_ action: @escaping () -> Void) {
        confirmActionBlock = action
    }
    
    private func setupContainers() {
        addContainerView()
        addActionContainerView()
    }
    
    private func addContainerView() {
        containerView.backgroundColor = UIColor.youniqClear
        containerView.layer.cornerRadius = Sizes.View.CornerRadius.regular
        containerView.clipsToBounds = true
        
        addSubview(containerView)
    }
    
    private func addActionContainerView() {
        actionContainerView.backgroundColor = UIColor.youniqWhite
        
        containerView.addSubview(actionContainerView)
    }
    
    private func addCancelButton() {
        cancelButton.title = "Cancel"
        cancelButton.normalColor = UIColor.youniqGrey
        cancelButton.addAction {
            self.cancelActionBlock?()
        }
        
        actionContainerView.addSubview(cancelButton)
    }
    
    private func addConfirmButton() {
        confirmButton.title = "Confirm"
        confirmButton.normalColor = UIColor.youniqBlue
        confirmButton.addAction {
            self.confirmActionBlock?()
        }
        
        actionContainerView.addSubview(confirmButton)
    }
    
    private func setupConstraints() {
        containerView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.size.equalTo(self.size)
        }
        
        actionContainerView.snp.makeConstraints { (make) in
            make.left.bottom.right.equalToSuperview()
            make.height.equalTo(Constants.customActionHeight)
        }
        
        confirmButton.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().inset(Margins.big)
        }
        
        cancelButton.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(Margins.big)
        }
        
        childView?.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview()
            make.bottom.equalTo(actionContainerView.snp.top)
        }
    }
}
