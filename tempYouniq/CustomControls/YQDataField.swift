//
//  YQDataField.swift
//  tempYouniq
//

import UIKit
import SnapKit

private struct Constants {
    static let cornerRadius: CGFloat = 7
    static let alphaComponent: CGFloat = 0.08
    static let invalidImageViewHeight: CGFloat = 16
    static let invalidImageViewOffset: CGFloat = -8
}

class YQDataField: UIView {
    
    private let titleLabel: YQLabel = YQLabel()
    private let inputField: YQTextField = YQTextField()
    private let invalidImageView: UIImageView = UIImageView(image: UIImage(named: "warningImage"))
    
    var title: String = "" {
        didSet {
            titleLabel.text = title
        }
    }
    
    var data: String = "" {
        didSet {
            inputField.text = data
        }
    }
    
    var isValid: Bool = true {
        didSet {
            if !isValid {
                self.backgroundColor = UIColor.youniqRed.withAlphaComponent(Constants.alphaComponent)
                self.invalidImageView.isHidden = false
            }
        }
    }
    
    public init() {
        super.init(frame: CGRect.zero)
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        
        layer.cornerRadius = Constants.cornerRadius
        
        addTitleLabel()
        addInputField()
        addInvalidImageView()
        
        setupConstraints()
    }
    
    private func addTitleLabel() {
        titleLabel.textColor = UIColor.youniqOrange
        titleLabel.font = UIFont.systemFont(ofSize: FontSize.small)
        
        addSubview(titleLabel)
    }
    
    private func addInputField() {
        addSubview(inputField)
    }
    
    private func addInvalidImageView() {
        invalidImageView.isHidden = true
        invalidImageView.contentMode = .scaleAspectFit
        
        addSubview(invalidImageView)
    }
    
    private func setupConstraints() {
        titleLabel.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview().offset(Margins.regular)
            make.height.equalTo(Sizes.Label.Height.small)
        }
        
        inputField.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(Margins.tiny)
            make.left.equalTo(titleLabel.snp.left)
            make.height.equalTo(Sizes.Label.Height.regular)
        }
        
        invalidImageView.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview().offset(Constants.invalidImageViewOffset)
            make.width.height.equalTo(Constants.invalidImageViewHeight)
        }
    }
}
