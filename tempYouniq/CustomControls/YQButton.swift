//
//  YQButton.swift
//  tempYouniq
//

import UIKit

private struct Constants {
    static let standardCornerRadius: CGFloat = 8
}

class YQButton: UIButton {
    
    override var isHighlighted: Bool {
        didSet {
            didChangeHighlighted()
        }
    }
    
    override var isEnabled: Bool {
        didSet {
            if !isEnabled {
                backgroundColor = UIColor.youniqLightGrey
                normalColor = UIColor.gray
            }
        }
    }
    
    private var actionBlock: (() -> Void)? = nil
    
    var normalColor: UIColor = UIColor.clear {
        didSet {
            setTitleColor(normalColor, for: .normal)
        }
    }
    
    var highlightedColor: UIColor = UIColor.clear {
        didSet {
            setTitleColor(highlightedColor, for: .highlighted)
        }
    }
    
    var disabledColor: UIColor = UIColor.clear {
        didSet {
            setTitleColor(disabledColor, for: .disabled)
        }
    }
    
    //    TODO: Implement once font is determined
//    var titleFont: UIFont? = UIFont.avenirMedium(ofSize: FontSize.medium) {
//        didSet {
//            titleLabel?.font = titleFont
//        }
//    }
    
    var title: String? {
        set {
            setTitle(newValue, for: .normal)
        }
        get {
            return currentTitle
        }
    }
    
    var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    static func standard() -> YQButton {
        let button: YQButton = YQButton()
        button.normalColor = UIColor.youniqWhite
        button.backgroundColor = UIColor.youniqBlue
        button.cornerRadius = Constants.standardCornerRadius
        
        return button
    }
    
    public init() {
        super.init(frame: CGRect.zero)
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func addAction(_ action: @escaping () -> Void) {
        actionBlock = action
    }
    
    func setup() {
        addTarget(self, action: #selector(didTouch), for: .touchUpInside)
    }
    
    @objc func didTouch() {
        actionBlock?()
    }
    
    func didChangeHighlighted() {
        if isHighlighted {
            
        }
        else {
    
        }
    }
}
