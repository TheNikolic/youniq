//
//  SelectPurchaseViewController.swift
//  tempYouniq
//

import UIKit
import SnapKit

private struct Constants {
    static let rowHeight: CGFloat = 96
    static let dateModalViewSize: CGSize = CGSize(width: 280, height: 240)
}

class SelectPurchaseViewController: BaseViewController {
    
    private let purchaseTableView: UITableView = UITableView()
    
    var dataSource: [Selection] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addPurchaseTableView()
        
        setupConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupDataSource()
    }
    
    private func setupDataSource() {
        let dataProvider = DataProvider()
        dataSource = dataProvider.getSelections()
    }
    
    private func addPurchaseTableView() {
        purchaseTableView.dataSource = self
        purchaseTableView.delegate = self
        purchaseTableView.register(SelectPurchesTableViewCell.self, forCellReuseIdentifier: SelectPurchesTableViewCell.reuseIdentifier)
        purchaseTableView.separatorStyle = .none
        purchaseTableView.showsVerticalScrollIndicator = false
        purchaseTableView.rowHeight = Constants.rowHeight
        
        view.addSubview(purchaseTableView)
    }
    
    private func setupConstraints() {
        purchaseTableView.snp.makeConstraints { (make) in
            if #available(iOS 11, *) {
                make.edges.equalTo(view.safeAreaLayoutGuide)
            } else {
                make.edges.equalToSuperview()
            }
        }
    }
}

extension SelectPurchaseViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SelectPurchesTableViewCell.reuseIdentifier, for: indexPath)
        
        guard let itemCell = cell as? SelectPurchesTableViewCell else {
            return cell
        }
        
        itemCell.cellImageName = dataSource[indexPath.row].cellImageName
        itemCell.title = dataSource[indexPath.row].title
        itemCell.name = dataSource[indexPath.row].name
        itemCell.date = dataSource[indexPath.row].date
        itemCell.price = dataSource[indexPath.row].price
        
        return itemCell
    }
}

extension SelectPurchaseViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectionView: SelectPurchaseModalView = SelectPurchaseModalView()
        selectionView.cellImageName  = dataSource[indexPath.row].cellImageName
        selectionView.title = dataSource[indexPath.row].title
        selectionView.date = dataSource[indexPath.row].date
        selectionView.price = dataSource[indexPath.row].price
        
        selectionView.snp.makeConstraints({ (make) in
            make.size.equalTo(Constants.dateModalViewSize)
        })
        
        let modalView = YQModalView.viewWith(childView: selectionView)
        modalView.layoutWith(size: Constants.dateModalViewSize)
        
        modalView.addConfirmAction {
            UIApplication.hideFullscreen(modalView: modalView)
        }
        
        modalView.addCancelAction {
            UIApplication.hideFullscreen(modalView: modalView)
        }
        
        UIApplication.showFullscreen(modalView: modalView)
    }
}
