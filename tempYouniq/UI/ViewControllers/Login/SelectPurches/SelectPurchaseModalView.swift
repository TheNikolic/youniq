//
//  SelectPurchaseModalView.swift
//  tempYouniq
//

import UIKit
import SnapKit

private struct Constants {
    static let cellImageSize: CGSize = CGSize(width: 110, height: 110)
    static let shadowOffset: CGSize = CGSize(width: 0, height: 2)
    static let shadowOpacity: Float = 0.2
    static let shadowRadius: CGFloat = 3
}

class SelectPurchaseModalView: UIView {
    
    private let headerLabel: YQLabel = YQLabel()
    private let headerSeparatorView: UIView = UIView()
    private let cellImageView: UIImageView = UIImageView()
    private let titleLabel: YQLabel = YQLabel.multiline()
    private let dateLabel: YQLabel = YQLabel()
    private let priceTitleLabel: YQLabel = YQLabel()
    private let priceLabel: YQLabel = YQLabel()
    private let bottomSeparatorView: UIView = UIView()
    
    var cellImageName: String = "" {
        didSet {
            cellImageView.image = UIImage(named: cellImageName)
        }
    }
    
    var title: String = "" {
        didSet {
            titleLabel.text = title
        }
    }
    
    var date: String = "" {
        didSet {
            dateLabel.text = date
        }
    }
    
    var price: String = "" {
        didSet {
            priceLabel.text = "$\(price)"
        }
    }
    
    public init() {
        super.init(frame: CGRect.zero)
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        
        backgroundColor = UIColor.youniqWhite
        
        addHeaderLabel()
        addHeaderSeparatorView()
        addCellImageView()
        addTitleLabel()
        addDateLabel()
        addPriceTitleLabel()
        addPriceLabel()
        addBottomSeparatorView()
        
        setupConstraints()
    }
    
    private func addHeaderLabel() {
        headerLabel.text = "Confirm Purchase"
        
        addSubview(headerLabel)
    }
    
    private func addHeaderSeparatorView() {
        headerSeparatorView.backgroundColor = UIColor.youniqLightGrey
        
        addSubview(headerSeparatorView)
    }
    
    private func addCellImageView() {
        cellImageView.contentMode = .scaleAspectFill
        cellImageView.clipsToBounds = true
        
        addSubview(cellImageView)
    }
    
    private func addTitleLabel() {
        titleLabel.font = UIFont.systemFont(ofSize: FontSize.regular)
        sizeToFit()
        
        addSubview(titleLabel)
    }
    
    private func addDateLabel() {
        dateLabel.font = UIFont.systemFont(ofSize: FontSize.teeny)
        dateLabel.textColor = UIColor.gray
        
        addSubview(dateLabel)
    }
    
    private func addPriceTitleLabel() {
        priceTitleLabel.text = "Total price"
        priceTitleLabel.font = UIFont.systemFont(ofSize: FontSize.teeny)
        priceTitleLabel.textColor = UIColor.gray
        
        addSubview(priceTitleLabel)
    }
    
    private func addPriceLabel() {
        priceLabel.font = UIFont.systemFont(ofSize: FontSize.big)
        priceLabel.textColor = UIColor.youniqBlack
        
        addSubview(priceLabel)
    }
    
    private func addBottomSeparatorView() {
        bottomSeparatorView.backgroundColor = UIColor.youniqLightGrey
        
        addSubview(bottomSeparatorView)
    }
    
    private func setupConstraints() {
        headerLabel.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview().offset(Margins.regular)
            make.height.equalTo(Sizes.Label.Height.regular)
        }
        
        headerSeparatorView.snp.makeConstraints { (make) in
            make.top.equalTo(headerLabel.snp.bottom).offset(Margins.regular)
            make.left.right.equalToSuperview()
            make.height.equalTo(Sizes.View.Separator.smallHeight)
        }
        
        cellImageView.snp.makeConstraints { (make) in
            make.top.equalTo(headerSeparatorView.snp.bottom).offset(Margins.regular)
            make.left.equalToSuperview().offset(Margins.regular)
            make.size.equalTo(Constants.cellImageSize)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(cellImageView.snp.top)
            make.left.equalTo(cellImageView.snp.right).offset(Margins.regular)
            make.right.equalToSuperview().inset(Margins.regular)
            //make.height.equalTo(Sizes.Label.Height.small)
        }
        
        dateLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(Margins.tiny)
            make.left.equalTo(titleLabel.snp.left)
            make.height.equalTo(Sizes.Label.Height.small)
        }
        
        priceLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(cellImageView.snp.bottom)
            make.left.equalTo(titleLabel.snp.left)
            make.height.equalTo(Sizes.Label.Height.regular)
        }
        
        priceTitleLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(priceLabel.snp.top).inset(Margins.tiny * -1)
            make.left.equalTo(titleLabel.snp.left)
            make.height.equalTo(Sizes.Label.Height.small)
        }
        
        bottomSeparatorView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(Sizes.View.Separator.smallHeight)
        }
    }
}
