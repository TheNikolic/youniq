//
//  SelectPurchesTableViewCell.swift
//  tempYouniq
//

import UIKit
import SnapKit

private struct Constants {
    static let cellImageHeight: CGFloat = 25
    static let shadowOffset: CGSize = CGSize(width: 0, height: 2)
    static let shadowOpacity: Float = 0.2
    static let shadowRadius: CGFloat = 3
    static let videoIconImageViewHeight: CGFloat = 14
}

class SelectPurchesTableViewCell: UITableViewCell {
    
    private let containerView: UIView = UIView()
    private let cellImageView: UIImageView = UIImageView()
    private let titleLabel: YQLabel = YQLabel()
    private let nameLabel: YQLabel = YQLabel()
    private let videoIconImageView: UIImageView = UIImageView(image: UIImage(named: "videoImage"))
    private let dateLabel: YQLabel = YQLabel()
    private let priceLabel: YQLabel = YQLabel()
    
    var cellImageName: String = "" {
        didSet {
            cellImageView.image = UIImage(named: cellImageName)
        }
    }
    
    var title: String = "" {
        didSet {
            titleLabel.text = title
        }
    }
    
    var name: String = "" {
        didSet {
            nameLabel.text = name
        }
    }
    
    var date: String = "" {
        didSet {
            dateLabel.text = date
        }
    }
    
    var price: String = "" {
        didSet {
            priceLabel.text = "$\(price)"
        }
    }
    
    static let reuseIdentifier: String = "SelectPurchesTableViewCell"
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        
        addContainerView()
        addCellImageView()
        addTitleLabel()
        addNameLabel()
        addVideoIconImageView()
        addDateLabel()
        addPriceLabel()
       

        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addContainerView() {
        containerView.backgroundColor = UIColor.youniqWhite
        containerView.layer.cornerRadius = Sizes.View.CornerRadius.small
        containerView.layer.shadowColor = UIColor.youniqBlack.cgColor
        containerView.layer.shadowOffset = Constants.shadowOffset
        containerView.layer.shadowRadius = Constants.shadowRadius
        containerView.layer.shadowOpacity = Constants.shadowOpacity
        
        contentView.addSubview(containerView)
    }
    
    private func addCellImageView() {
        cellImageView.contentMode = .scaleAspectFill
        cellImageView.clipsToBounds = true
        
        containerView.addSubview(cellImageView)
    }
    
    private func addTitleLabel() {
        titleLabel.font = UIFont.systemFont(ofSize: FontSize.regular)
        
        containerView.addSubview(titleLabel)
    }
    
    private func addNameLabel() {
        nameLabel.font = UIFont.systemFont(ofSize: FontSize.teeny)
        nameLabel.textColor = UIColor.gray
        
        containerView.addSubview(nameLabel)
    }
    
    private func addVideoIconImageView() {
        containerView.addSubview(videoIconImageView)
    }
    
    private func addDateLabel() {
        dateLabel.font = UIFont.systemFont(ofSize: FontSize.teeny)
        dateLabel.textColor = UIColor.gray
        
        containerView.addSubview(dateLabel)
    }
    
    private func addPriceLabel() {
        priceLabel.font = UIFont.systemFont(ofSize: FontSize.teeny)
        priceLabel.textColor = UIColor.youniqBlue
        
        containerView.addSubview(priceLabel)
    }
    
    private func setupConstraints() {
        containerView.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview().offset(Margins.tiny)
            make.right.equalToSuperview().inset(Margins.tiny)
            make.bottom.equalToSuperview().inset(Margins.small)
        }
        
        cellImageView.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview()
            make.width.height.equalTo(containerView.snp.height)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(Margins.regular)
            make.left.equalTo(cellImageView.snp.right).offset(Margins.big)
            make.height.equalTo(Sizes.Label.Height.small)
        }
        
        nameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(Margins.tiny)
            make.left.equalTo(titleLabel.snp.left)
            make.height.equalTo(Sizes.Label.Height.small)
        }
        
        videoIconImageView.snp.makeConstraints { (make) in
            make.top.equalTo(nameLabel.snp.bottom).offset(Margins.regular)
            make.left.equalTo(nameLabel.snp.left)
            make.width.height.equalTo(Constants.videoIconImageViewHeight)
        }
        
        dateLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(videoIconImageView.snp.centerY)
            make.left.equalTo(videoIconImageView.snp.right).offset(Margins.regular)
            make.height.equalTo(Sizes.Label.Height.small)
        }
        
        priceLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(dateLabel.snp.centerY)
            make.right.equalToSuperview().inset(Margins.regular)
            make.height.equalTo(Sizes.Label.Height.small)
        }
    }
}
