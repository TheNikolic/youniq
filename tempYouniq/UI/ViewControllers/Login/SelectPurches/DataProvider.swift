//
//  DataProvider.swift
//  tempYouniq
//

import Foundation

class DataProvider {
    
    func getSelections() -> [Selection] {
        
        let selection1 = Selection()
        selection1.cellImageName = "wiredMagazine"
        selection1.title = "Wired Magazine"
        selection1.name = "Will Farrell"
        selection1.date = "Feb 2018"
        selection1.price = "16.99"
        
        let selection2 = Selection()
        selection2.cellImageName = "forbesMagazine"
        selection2.title = "Time Magazine"
        selection2.name = "Jessica Alba"
        selection2.date = "Jan 2018"
        selection2.price = "12.99"
        
        let selection3 = Selection()
        selection3.cellImageName = "esquireMagazine"
        selection3.title = "Esquire Magazine"
        selection3.name = "Will Smith "
        selection3.date = "Feb 2018"
        selection3.price = "16.99"
        
        let selection4 = Selection()
        selection4.cellImageName = "foodForGood"
        selection4.title = "Food For Good"
        selection4.name = "Kristina Matthew"
        selection4.date = "Feb 2018"
        selection4.price = "22.99"
        
        let selection5 = Selection()
        selection5.cellImageName = "vintageRaceCars"
        selection5.title = "Vintage Race Cars"
        selection5.name = "Johan Phelps"
        selection5.date = "Feb 2018"
        selection5.price = "18.99"
        
        let selection6 = Selection()
        selection6.cellImageName = "photography101"
        selection6.title = "Photography 101"
        selection6.name = "Herbert Murphy"
        selection6.date = "Feb 2018"
        selection6.price = "11.99"
        
        let selection7 = Selection()
        selection7.cellImageName = "historyOfPhotography"
        selection7.title = "History Of Photography"
        selection7.name = "Dominic Stevens"
        selection7.date = "Feb 2018"
        selection7.price = "8.99"

        return [selection1, selection2, selection3, selection4, selection5, selection6, selection7]
    }
}

class Selection {
    
    var cellImageName: String = ""
    var title: String = ""
    var name: String = ""
    var date: String = ""
    var price: String = ""
}
