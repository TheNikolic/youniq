//
//  ScanIDViewController.swift
//  tempYouniq
//

import UIKit
import SnapKit

private struct Constants {
    static let captureFrameOffset: CGFloat = 60
    static let captureFrameHeight: CGFloat = 206
    static let infoLabelTopOffset: CGFloat = 84
    static let infoLabelSideOffset: CGFloat = 64
    static let infoLabelCornerRadius: CGFloat = 16
    static let bottomContainerViewHeight: CGFloat = 124
    static let captureButtonOuterRingViewHeight: CGFloat = 66
    static let outerRingViewBorderWidth: CGFloat = 6
    static let captureButtonHeight: CGFloat = 50
    static let captureFrameCornerRadius: CGFloat = 18
}

class ScanIDViewController: BaseViewController {
    
    private let captureFrame: UIImageView = UIImageView()
    private let infoLabel: YQLabel = YQLabel()
    private let bottomContainerView: UIView = UIView()
    private let captureButtonOuterRingView: UIView = UIView()
    private let captureButton: YQButton = YQButton()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.youniqGrey
        title = "Scan your ID"
        
        addCaptureFrame()
        addInfoLabel()
        addBottomContainerView()
        addCaptureButtonOuterRingView()
        addCaptureButton()
        
        setupConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavigationBar()
    }
    
    private func setupNavigationBar() {
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.tintColor = UIColor.youniqWhite
        //navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.tempYouniqWhite]
    }
    
    private func addCaptureFrame() {
        captureFrame.backgroundColor = UIColor.youniqClear
        captureFrame.layer.borderColor = UIColor.youniqWhite.cgColor
        captureFrame.layer.borderWidth = Sizes.BorderWidth.regular
        captureFrame.layer.cornerRadius = Constants.captureFrameCornerRadius
        
        view.addSubview(captureFrame)
    }
    
    private func addInfoLabel() {
        infoLabel.text = "Take a picture of the front side"
        infoLabel.textAlignment = .center
        infoLabel.textColor = UIColor.youniqBlack
        infoLabel.backgroundColor = UIColor.youniqWhite
        infoLabel.layer.cornerRadius = Constants.infoLabelCornerRadius
        infoLabel.layer.masksToBounds = true
        
        view.addSubview(infoLabel)
    }
    
    private func addBottomContainerView() {
        bottomContainerView.backgroundColor = UIColor.youniqBlack
        
        view.addSubview(bottomContainerView)
    }
    
    private func addCaptureButtonOuterRingView() {
        captureButtonOuterRingView.backgroundColor = UIColor.clear
        captureButtonOuterRingView.layer.cornerRadius = Constants.captureButtonOuterRingViewHeight / 2
        captureButtonOuterRingView.layer.borderWidth = Constants.outerRingViewBorderWidth
        captureButtonOuterRingView.layer.borderColor = UIColor.youniqWhite.cgColor
        
        
        bottomContainerView.addSubview(captureButtonOuterRingView)
    }
    
    private func addCaptureButton() {
        captureButton.backgroundColor = UIColor.youniqWhite
        captureButton.cornerRadius = Sizes.Button.Height.regular / 2
        captureButton.addAction {
            self.navigationController?.pushViewController(VerifyAccountDataViewController(), animated: true)
        }
        
        bottomContainerView.addSubview(captureButton)
    }
    
    private func setupConstraints() {
        captureFrame.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview().inset(Constants.captureFrameOffset * -1)
            make.left.equalToSuperview().offset(Margins.big)
            make.right.equalToSuperview().inset(Margins.big)
            make.height.equalTo(Constants.captureFrameHeight)
        }
        
        infoLabel.snp.makeConstraints { (make) in
            make.top.equalTo(captureFrame.snp.bottom).offset(Constants.infoLabelTopOffset)
            make.left.equalToSuperview().offset(Constants.infoLabelSideOffset)
            make.right.equalToSuperview().inset(Constants.infoLabelSideOffset)
            make.height.equalTo(Sizes.Label.Height.medium)
        }
        
        bottomContainerView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(Constants.bottomContainerViewHeight)
        }
        
        captureButtonOuterRingView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.height.width.equalTo(Constants.captureButtonOuterRingViewHeight)
        }
        
        captureButton.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.height.width.equalTo(Sizes.Button.Height.regular)
        }
    }
    
}
