//
//  CaptureSelfieViewController.swift
//  tempYouniq
//

import UIKit
import SnapKit

class CaptureSelfieViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Take a selfie"
        
        view.addTap {
            self.navigationController?.pushViewController(FollowDotViewController(), animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.tintColor = nil
    }
}
