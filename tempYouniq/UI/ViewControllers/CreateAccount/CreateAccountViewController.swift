//
//  CreateAccountViewController.swift
//  tempYouniq
//

import UIKit
import SnapKit

private struct Constants {
    static let titleLabelOffset: CGFloat = 98
    static let titleLabelHeight: CGFloat = 150
    static let imageViewHeight: CGFloat = 70
    static let startBottomOffset: CGFloat = 70
}

class CreateAccountViewController: BaseViewController {
    
    private let titleLabel: YQLabel = YQLabel.multiline()
    private let scanImageView: UIImageView = UIImageView(image: UIImage(named: "scanIDImage"))
    private let scanLabel: YQLabel = YQLabel.multiline()
    private let selfieImageView: UIImageView = UIImageView(image: UIImage(named: "selfieImage"))
    private let selfieLabel: YQLabel = YQLabel.multiline()
    private let startButton: YQButton = YQButton.standard()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Create account"
        
        addTitleLabel()
        addScanImageView()
        addScanLabel()
        addSelfieImageView()
        addSelfieLabel()
        addStartButton()
        
        setupConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.tintColor = nil
        //navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.tempYouniqBlack]
    }
    
    private func addTitleLabel() {
        titleLabel.text = "In order to onboard as a new client, you will be asked to perform these two steps:"
        titleLabel.font = UIFont.boldSystemFont(ofSize: FontSize.extraLarge)
        titleLabel.textAlignment = .center        
        
        view.addSubview(titleLabel)
    }
    
    private func addScanImageView() {
        scanImageView.contentMode = .scaleAspectFit
        
        view.addSubview(scanImageView)
    }
    
    private func addScanLabel() {
        scanLabel.text = "Scan your ID \nTake a picture of both sides of your ID"
        
        view.addSubview(scanLabel)
    }
    
    private func addSelfieImageView() {
        selfieImageView.contentMode = .scaleAspectFit
        
        view.addSubview(selfieImageView)
    }
    
    private func addSelfieLabel() {
        selfieLabel.text = "Take a selfie \nTake a picture of your face to verify"
        
        view.addSubview(selfieLabel)
    }
    
    private func addStartButton() {
        startButton.title = "Start"
        startButton.addAction {
            self.navigationController?.pushViewController(ScanIDViewController(), animated: true)
        }
        
        view.addSubview(startButton)
    }
    
    private func setupConstraints() {
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(Constants.titleLabelOffset)
            make.left.equalToSuperview().offset(Margins.extraLarge)
            make.right.equalToSuperview().inset(Margins.extraLarge)
            make.height.equalTo(Constants.titleLabelHeight)
        }
        
        scanImageView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(Margins.extraBig)
            make.left.equalTo(titleLabel.snp.left)
            make.width.height.equalTo(Constants.imageViewHeight)
        }
        
        scanLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(scanImageView.snp.centerY)
            make.left.equalTo(scanImageView.snp.right).offset(Margins.medium)
            make.right.equalTo(titleLabel.snp.right).inset(Margins.small)
            make.height.equalTo(scanImageView.snp.height)
        }
        
        selfieImageView.snp.makeConstraints { (make) in
            make.top.equalTo(scanImageView.snp.bottom).offset(Margins.extraLarge)
            make.left.equalTo(scanImageView.snp.left)
            make.width.height.equalTo(Constants.imageViewHeight)
        }
        
        selfieLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(selfieImageView.snp.centerY)
            make.left.equalTo(selfieImageView.snp.right).offset(Margins.medium)
            make.right.equalTo(titleLabel.snp.right).inset(Margins.small)
            make.height.equalTo(selfieImageView.snp.height)
        }
        
        startButton.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Margins.extraLarge)
            make.right.equalToSuperview().inset(Margins.extraLarge)
            make.bottom.equalToSuperview().inset(Constants.startBottomOffset)
            make.height.equalTo(Sizes.Button.Height.regular)
        }
    }
}

