//
//  VerifyAccountDataViewController.swift
//  tempYouniq
//

import UIKit
import SnapKit

private struct Constants {
    static let documentImageViewSize: CGSize = CGSize(width: 138, height: 88)
    static let documentImageViewCornerRadius: CGFloat = 4
    static let documentImageViewShadowOpacity: Float = 0.3
    static let dataFieldSize: CGSize = CGSize(width: 150, height: 60)
}

class VerifyAccountDataViewController: BaseViewController {
    
    private let verifyLabel: YQLabel = YQLabel.multiline()
    private let titleLabel: YQLabel = YQLabel.multiline()
    private let documentImageView: UIImageView = UIImageView()
    private let detailsLabel: YQLabel = YQLabel.multiline()
    private let lastNameField: YQDataField = YQDataField()
    private let firstNameField: YQDataField = YQDataField()
    private let personalNoField: YQDataField = YQDataField()
    private let genderField: YQDataField = YQDataField()
    private let dateOfBirthField: YQDataField = YQDataField()
    private let cityField: YQDataField = YQDataField()
    private let acceptButton: YQButton = YQButton.standard()
    
    var dataIsValid: Bool = true {
        didSet {
            if !dataIsValid {
                verifyLabel.backgroundColor = UIColor.youniqRed
                verifyLabel.text = "\(numberOfUncertainItems)" + " " + "uncertain items"
                acceptButton.isEnabled = false
            } else {
                verifyLabel.backgroundColor = UIColor.youniqBlack
                verifyLabel.text = "All items recognized"
                acceptButton.isEnabled = true
            }
        }
    }
    
    var numberOfUncertainItems: Int = 1 {
        didSet {
            verifyLabel.text = "\(numberOfUncertainItems)" + " " + "uncertain items"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataIsValid = true
        
        addVerifyLabel()
        addTitleLabel()
        addDocumentImageView()
        addDetailsLabel()
        addLastNameField()
        addFirstNameField()
        addPersonalNoField()
        addGenderField()
        addDateOfBirthField()
        addCityField()
        addAcceptButton()
        
        setupConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true
    }
    
    private func addVerifyLabel() {
        verifyLabel.textColor = UIColor.youniqWhite
        verifyLabel.textAlignment = .center
        
        view.addSubview(verifyLabel)
    }
    
    private func addTitleLabel() {
        titleLabel.text = "Please verify \nyour data"
        titleLabel.font = UIFont.systemFont(ofSize: FontSize.large)
        titleLabel.textColor = UIColor.youniqBlack
        
        view.addSubview(titleLabel)
    }
    
    private func addDocumentImageView() {
        documentImageView.backgroundColor = UIColor.youniqWhite
        documentImageView.layer.cornerRadius = Constants.documentImageViewCornerRadius
        documentImageView.layer.shadowColor = UIColor.youniqBlack.cgColor
        documentImageView.layer.shadowOffset = Sizes.View.Shadow.shadowOffset
        documentImageView.layer.shadowRadius = Sizes.View.Shadow.shadowRadius
        documentImageView.layer.shadowOpacity = Constants.documentImageViewShadowOpacity
        
        view.addSubview(documentImageView)
    }
    
    private func addDetailsLabel() {
        detailsLabel.backgroundColor = UIColor.lightGray
        detailsLabel.text = "Personal details"
        detailsLabel.textColor = UIColor.youniqWhite
        detailsLabel.textAlignment = .center
        
        view.addSubview(detailsLabel)
    }
    
    private func addLastNameField() {
        lastNameField.title = "Last name"
        
        view.addSubview(lastNameField)
    }
    
    private func addFirstNameField() {
        firstNameField.title = "First name"
        firstNameField.isValid = false
        
        view.addSubview(firstNameField)
    }
    
    private func addPersonalNoField() {
        personalNoField.title = "Personal No."
        personalNoField.data = "750303-1234"
        
        view.addSubview(personalNoField)
    }
    
    private func addGenderField() {
        genderField.title = "Gender"
        
        view.addSubview(genderField)
    }
    
    private func addDateOfBirthField() {
        dateOfBirthField.title = "Date of birth"
        
        view.addSubview(dateOfBirthField)
    }
    
    private func addCityField() {
        cityField.title = "City"
        
        view.addSubview(cityField)
    }
    
    private func addAcceptButton() {
        acceptButton.title = "I accept"
        acceptButton.addAction {
            self.navigationController?.pushViewController(TakeSelfieViewController(), animated: true)
        }
        
        view.addSubview(acceptButton)
    }
    
    private func setupConstraints() {
        verifyLabel.snp.makeConstraints { (make) in
            if #available(iOS 11, *) {
                make.left.top.right.equalTo(view.safeAreaLayoutGuide)
                make.height.equalTo(Sizes.Label.Height.medium)
            } else {
                make.left.top.right.equalToSuperview()
                make.height.equalTo(Sizes.Label.Height.medium)
            }
        }
        
        documentImageView.snp.makeConstraints { (make) in
            make.top.equalTo(verifyLabel.snp.bottom).offset(Margins.large)
            make.right.equalToSuperview().inset(Margins.large)
            make.size.equalTo(Constants.documentImageViewSize)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Margins.large)
            make.top.bottom.equalTo(documentImageView)
            make.right.equalTo(documentImageView.snp.left)
        }
        
        detailsLabel.snp.makeConstraints { (make) in
            make.top.equalTo(documentImageView.snp.bottom).offset(Margins.large)
            make.left.right.equalToSuperview()
            make.height.equalTo(Sizes.Label.Height.medium)
        }
        
        lastNameField.snp.makeConstraints { (make) in
            make.top.equalTo(detailsLabel.snp.bottom).offset(Margins.big)
            make.left.equalToSuperview().offset(Margins.large)
            make.size.equalTo(Constants.dataFieldSize)
        }
        
        firstNameField.snp.makeConstraints { (make) in
            make.top.equalTo(lastNameField.snp.top)
            make.right.equalToSuperview().inset(Margins.large)
            make.size.equalTo(Constants.dataFieldSize)
        }
        
        personalNoField.snp.makeConstraints { (make) in
            make.top.equalTo(lastNameField.snp.bottom).offset(Margins.medium)
            make.left.equalTo(lastNameField.snp.left)
            make.size.equalTo(Constants.dataFieldSize)
        }
        
        genderField.snp.makeConstraints { (make) in
            make.top.equalTo(personalNoField.snp.top)
            make.right.equalTo(firstNameField.snp.right)
            make.size.equalTo(Constants.dataFieldSize)
        }
        
        dateOfBirthField.snp.makeConstraints { (make) in
            make.top.equalTo(personalNoField.snp.bottom).offset(Margins.medium)
            make.left.equalTo(lastNameField.snp.left)
            make.size.equalTo(Constants.dataFieldSize)
        }
        
        cityField.snp.makeConstraints { (make) in
            make.top.equalTo(dateOfBirthField.snp.top)
            make.right.equalTo(firstNameField.snp.right)
            make.size.equalTo(Constants.dataFieldSize)
        }
        
        acceptButton.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().inset(Margins.large)
            make.left.equalToSuperview().offset(Margins.extraLarge)
            make.right.equalToSuperview().inset(Margins.extraLarge)
            make.height.equalTo(Sizes.Button.Height.regular)
        }
    }
}
