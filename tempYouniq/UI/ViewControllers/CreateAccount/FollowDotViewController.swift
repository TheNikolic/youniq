//
//  FollowDotViewController.swift
//  tempYouniq
//

import UIKit
import SnapKit

private struct Constants {
    static let dotViewHeight: CGFloat = 46
    static let dotInnerRingViewHeight: CGFloat = 20
    static let dotViewBorderWidth: CGFloat = 9
}

class FollowDotViewController: BaseViewController {
    
    private let titleLabel: YQLabel = YQLabel()
    private let dotView: UIView = UIView()
    private let dotInnerRingView: UIView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addTitleLabel()
        addDotView()
        addDotInnerRingView()
        
        setupConstraints()
        
        view.addTap {
            let verifiedVC: VerifiedViewController = VerifiedViewController()
            verifiedVC.titleLabelText = "Velimir Karadzic"
            verifiedVC.successTitleLabelText = "Registration Successful"
            verifiedVC.successDescriptionLabelText = "You can now log in to your account"
            self.present(verifiedVC, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true
    }
    
    private func addTitleLabel() {
        titleLabel.text = "Please follow the dot"
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.systemFont(ofSize: FontSize.extraLarge)
        
        view.addSubview(titleLabel)
    }
    private func addDotView() {
        dotView.backgroundColor = UIColor.youniqClear
        dotView.layer.cornerRadius = Constants.dotViewHeight / 2
        dotView.layer.borderColor = UIColor.youniqOrange.cgColor
        dotView.layer.borderWidth = Constants.dotViewBorderWidth
        
        view.addSubview(dotView)
    }
    private func addDotInnerRingView() {
        dotInnerRingView.backgroundColor = UIColor.youniqOrange
        dotInnerRingView.layer.cornerRadius = Constants.dotInnerRingViewHeight / 2
        
        dotView.addSubview(dotInnerRingView)
    }
    
    private func setupConstraints() {
        titleLabel.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.height.equalTo(Sizes.Label.Height.big)
        }
        
        dotView.snp.makeConstraints { (make) in
            make.bottom.right.equalToSuperview().inset(Margins.extraLarge)
            make.width.height.equalTo(Constants.dotViewHeight)
        }
        
        dotInnerRingView.snp.makeConstraints { (make) in
            make.center.equalTo(dotView.snp.center)
            make.width.height.equalTo(Constants.dotInnerRingViewHeight)
        }
    }
}
