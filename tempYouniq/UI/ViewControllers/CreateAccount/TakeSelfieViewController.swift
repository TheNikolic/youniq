//
//  TakeSelfieViewController.swift
//  tempYouniq
//

import UIKit
import SnapKit

private struct Constants {
    static let titleLabelOffset: CGFloat = 62
    static let imageViewHeight: CGFloat = 70
    static let continueButtonOffset: CGFloat = 72
}

class TakeSelfieViewController: BaseViewController {
    
    private let titleLabel: YQLabel = YQLabel()
    private let faceImageView: UIImageView = UIImageView(image: UIImage(named: "selfieImage"))
    private let faceLabel: YQLabel = YQLabel()
    private let lightSourceImageView: UIImageView = UIImageView(image: UIImage(named: "lightSourceImage"))
    private let lightSourceLabel: YQLabel = YQLabel()
    private let verifideImageView: UIImageView = UIImageView(image: UIImage(named: "verifiedImage"))
    private let verifideLabel: YQLabel = YQLabel()
    private let continueButton: YQButton = YQButton.standard()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addTitleLabel()
        addFaceImageView()
        addFaceLabel()
        addLightSourceImageView()
        addLightSourceLabel()
        addVerifideImageView()
        addVerifideLabel()
        addContinueButton()
        
        setupConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true
    }
    
    private func addTitleLabel() {
        titleLabel.text = "Take a selfie"
        titleLabel.font = UIFont.systemFont(ofSize: FontSize.extraLarge)
        titleLabel.textAlignment = .center
        
        view.addSubview(titleLabel)
    }
    
    private func addFaceImageView() {
        faceImageView.contentMode = .scaleAspectFit
        
        view.addSubview(faceImageView)
    }
    
    private func addFaceLabel() {
        faceLabel.text = "Center your face into the circle"
        faceLabel.textAlignment = .center
        
        view.addSubview(faceLabel)
    }
    
    private func addLightSourceImageView() {
        lightSourceImageView.contentMode = .scaleAspectFit
        
        view.addSubview(lightSourceImageView)
    }
    
    private func addLightSourceLabel() {
        lightSourceLabel.text = "Rotate face towards light source"
        lightSourceLabel.textAlignment = .center
        
        view.addSubview(lightSourceLabel)
    }
    
    private func addVerifideImageView() {
        verifideImageView.contentMode = .scaleAspectFit
        
        view.addSubview(verifideImageView)
    }
    
    private func addVerifideLabel() {
        verifideLabel.text = "That's it!"
        verifideLabel.textAlignment = .center
        
        view.addSubview(verifideLabel)
    }
    
    private func addContinueButton() {
        continueButton.title = "Contnue"
        continueButton.addAction {
            self.navigationController?.pushViewController(CaptureSelfieViewController(), animated: true)
        }
        
        view.addSubview(continueButton)
    }
    
    private func setupConstraints() {
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(Constants.titleLabelOffset)
            make.left.right.equalToSuperview()
            make.height.equalTo(Sizes.Label.Height.big)
        }
        
        faceImageView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(Margins.extraBig)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(Constants.imageViewHeight)
        }
        
        faceLabel.snp.makeConstraints { (make) in
            make.top.equalTo(faceImageView.snp.bottom).offset(Margins.regular)
            make.left.right.equalToSuperview()
            make.height.equalTo(Sizes.Label.Height.small)
        }
        
        lightSourceImageView.snp.makeConstraints { (make) in
            make.top.equalTo(faceLabel.snp.bottom).offset(Margins.extraBig)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(Constants.imageViewHeight)
        }
        
        lightSourceLabel.snp.makeConstraints { (make) in
            make.top.equalTo(lightSourceImageView.snp.bottom).offset(Margins.regular)
            make.left.right.equalToSuperview()
            make.height.equalTo(Sizes.Label.Height.small)
        }
        
        verifideImageView.snp.makeConstraints { (make) in
            make.top.equalTo(lightSourceLabel.snp.bottom).offset(Margins.extraBig)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(Constants.imageViewHeight)
        }
        
        verifideLabel.snp.makeConstraints { (make) in
            make.top.equalTo(verifideImageView.snp.bottom).offset(Margins.regular)
            make.left.right.equalToSuperview()
            make.height.equalTo(Sizes.Label.Height.small)
        }
        
        continueButton.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().inset(Margins.large)
            make.left.equalToSuperview().offset(Margins.extraLarge)
            make.right.equalToSuperview().inset(Margins.extraLarge)
            make.height.equalTo(Sizes.Button.Height.regular)
        }
    }
}
