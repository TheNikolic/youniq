//
//  StartViewController.swift
//  tempYouniq
//

import UIKit
import SnapKit

private struct Constants {
    static let logoImageViewSize: CGSize = CGSize(width: 216, height: 60)
    static let logoImageViewTopOffset: CGFloat = 92
    static let customTitleLabelMargine: CGFloat = 50
    static let createAccountButtonBottomOffset: CGFloat = 70
}

class StartViewController: BaseViewController {
    
    private let logoImageView: UIImageView = UIImageView(image: UIImage(named: "logo"))
    private let titleLabel: YQLabel = YQLabel.multiline()
    private let createAccountButton: YQButton = YQButton.standard()
    private let loginButton: YQButton = YQButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addLogoImageView()
        addTitleLabel()
        addCreateAccountButton()
        addLoginButton()
        
        setupConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true
    }
    
    private func addLogoImageView() {
        logoImageView.contentMode = .scaleAspectFit
        
        view.addSubview(logoImageView)
    }
    
    private func addTitleLabel() {
        titleLabel.text = "Welcome to the future of secure and convinient transactions."
        titleLabel.textAlignment = .center
        
        view.addSubview(titleLabel)
    }
    
    private func addCreateAccountButton() {
        createAccountButton.title = "Create account"
        createAccountButton.addAction {
            self.navigationController?.pushViewController(CreateAccountViewController(), animated: true)
        }
        
        view.addSubview(createAccountButton)
    }
    
    private func addLoginButton() {
        loginButton.title = "Login"
        loginButton.normalColor = UIColor.youniqBlue
        loginButton.addAction {
            self.navigationController?.pushViewController(SelectPurchaseViewController(), animated: true)
        }
        
        view.addSubview(loginButton)
    }
    
    private func setupConstraints() {
        logoImageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(Constants.logoImageViewTopOffset)
            make.centerX.equalToSuperview()
            make.size.equalTo(Constants.logoImageViewSize)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview().inset(Margins.extraLarge * -1)
            make.left.equalToSuperview().offset(Constants.customTitleLabelMargine)
            make.right.equalToSuperview().inset(Constants.customTitleLabelMargine)
            make.height.equalTo(Sizes.Label.Height.big)
        }
        
        createAccountButton.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Margins.extraLarge)
            make.right.equalToSuperview().inset(Margins.extraLarge)
            make.bottom.equalToSuperview().inset(Constants.createAccountButtonBottomOffset)
            make.height.equalTo(Sizes.Button.Height.regular)
        }
        
        loginButton.snp.makeConstraints { (make) in
            make.top.equalTo(createAccountButton.snp.bottom).offset(Margins.medium)
            make.centerX.equalToSuperview()
            make.height.equalTo(Sizes.Button.Height.small)
        }
    }
}
