//
//  VerifiedViewController.swift
//  tempYouniq
//

import UIKit
import SnapKit

private struct Constants {
    static let backgroundUserImageViewHeight: CGFloat = 110
    static let backgroundUserImageViewOffset: CGFloat = 92
    static let userImageViewHeight: CGFloat = 100
    static let successContainerViewCornerRadius: CGFloat = 12
    static let successContainerViewHeight: CGFloat = 242
    static let successContainerViewShadowOpacity: Float = 0.1
    static let successImageViewOffset: CGFloat = 28
    static let successImageViewHeight: CGFloat = 82
    static let continueButtonOffset: CGFloat = 60
}

class VerifiedViewController: BaseViewController {
    
    private let backgroundUserImageView: UIView = UIView()
    private let userImageView: UIImageView = UIImageView(image: UIImage(named: "tempUserImage"))
    private let titleLabel: YQLabel = YQLabel()
    private let successContainerView: UIView = UIView()
    private let successImageView: UIImageView = UIImageView(image: UIImage(named: "verifiedImage"))
    private let successTitlelabel: YQLabel = YQLabel()
    private let successDescriptionLabel: YQLabel = YQLabel.multiline()
    private let continueButton: YQButton = YQButton.standard()
    
    var userImage: UIImage? {
        didSet {
            
        }
    }
    
    var titleLabelText: String = "" {
        didSet {
            titleLabel.text = titleLabelText
        }
    }
    
    var successTitleLabelText: String = "" {
        didSet {
            successTitlelabel.text = successTitleLabelText
        }
    }
    
    var successDescriptionLabelText: String = "" {
        didSet {
            successDescriptionLabel.text = successDescriptionLabelText
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addBackgroundUserImageView()
        addUserImageView()
        addTitleLabel()
        addSuccessContainerView()
        addSuccessImageView()
        addSuccessTitlelabel()
        addSuccessDescriptionLabel()
        addContinueButton()
        
        setupConstraints()
    }
    
    private func addBackgroundUserImageView() {
        backgroundUserImageView.backgroundColor = UIColor.youniqWhite
        backgroundUserImageView.layer.cornerRadius = Constants.backgroundUserImageViewHeight / 2
        backgroundUserImageView.layer.shadowColor = UIColor.youniqBlack .cgColor
        backgroundUserImageView.layer.shadowOffset = Sizes.View.Shadow.shadowOffset
        backgroundUserImageView.layer.shadowRadius = Sizes.View.Shadow.shadowRadius
        backgroundUserImageView.layer.shadowOpacity = Sizes.View.Shadow.shadowOpacity
        
        view.addSubview(backgroundUserImageView)
    }
    
    private func addUserImageView() {
        userImageView.contentMode = .scaleAspectFill
        userImageView.layer.cornerRadius = Constants.userImageViewHeight/2
        userImageView.layer.masksToBounds = false
        userImageView.clipsToBounds = true
        
        view.addSubview(userImageView)
    }
    
    private func addTitleLabel() {
        titleLabel.font = UIFont.systemFont(ofSize: FontSize.extraLarge)
        titleLabel.textAlignment = .center
        
        view.addSubview(titleLabel)
    }
    
    private func addSuccessContainerView() {
        successContainerView.backgroundColor = UIColor.youniqWhite
        successContainerView.layer.cornerRadius = Constants.successContainerViewCornerRadius
        successContainerView.layer.shadowColor = UIColor.youniqBlack.cgColor
        successContainerView.layer.shadowOffset = Sizes.View.Shadow.shadowOffset
        successContainerView.layer.shadowRadius = Sizes.View.Shadow.shadowRadius
        successContainerView.layer.shadowOpacity = Constants.successContainerViewShadowOpacity
        
        view.addSubview(successContainerView)
    }
    
    private func addSuccessImageView() {
        successImageView.contentMode = .scaleAspectFit
        
        successContainerView.addSubview(successImageView)
    }
    
    private func addSuccessTitlelabel() {
        successTitlelabel.font = UIFont.systemFont(ofSize: FontSize.normal)
        successTitlelabel.textAlignment = .center
        
        successContainerView.addSubview(successTitlelabel)
    }
    
    private func addSuccessDescriptionLabel() {
        successDescriptionLabel.font = UIFont.systemFont(ofSize: FontSize.regular)
        successDescriptionLabel.textAlignment = .center
        
        successContainerView.addSubview(successDescriptionLabel)
    }
    
    private func addContinueButton() {
        continueButton.title = "Continue"
        
        view.addSubview(continueButton)
    }
    
    private func setupConstraints() {
        backgroundUserImageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(Constants.backgroundUserImageViewOffset)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(Constants.backgroundUserImageViewHeight)
        }
        
        userImageView.snp.makeConstraints { (make) in
            make.center.equalTo(backgroundUserImageView.snp.center)
            make.width.height.equalTo(Constants.userImageViewHeight)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(backgroundUserImageView.snp.bottom).offset(Margins.small)
            make.left.right.equalToSuperview()
            make.height.equalTo(Sizes.Label.Height.big)
        }
        
        successContainerView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(Margins.large)
            make.left.equalToSuperview().offset(Margins.large)
            make.right.equalToSuperview().inset(Margins.large)
            make.height.equalTo(Constants.successContainerViewHeight)
        }
        
        successImageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(Constants.successImageViewOffset)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(Constants.successImageViewHeight)
        }
        
        successTitlelabel.snp.makeConstraints { (make) in
            make.top.equalTo(successImageView.snp.bottom).offset(Margins.medium)
            make.left.right.equalToSuperview().offset(Margins.big)
            make.right.equalToSuperview().inset(Margins.big)
            make.height.equalTo(Sizes.Label.Height.medium)
        }
        
        successDescriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(successTitlelabel.snp.bottom).offset(Margins.small)
            make.left.equalTo(successTitlelabel.snp.left).offset(Margins.big)
            make.right.equalTo(successTitlelabel.snp.right).inset(Margins.big)
            make.height.equalTo(Sizes.Label.Height.big)
        }
        
        continueButton.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().inset(Margins.large)
            make.left.equalToSuperview().offset(Margins.extraLarge)
            make.right.equalToSuperview().inset(Margins.extraLarge)
            make.height.equalTo(Sizes.Button.Height.regular)
        }
    }
}
